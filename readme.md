# Domaći zadatak

* Napraviti sistem za unos oglasa (opis, slika i cena dok se datum unosi automatski)
* Samo prijavljeni korisnici mogu postavljati oglase
* Podatke za logovanje uneti unapred u bazu podataka
* Lozinke kriptovati sa nekim od algoritama i koristiti SALT tehniku
* Obavezno koristiti sesije.
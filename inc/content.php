<?php

if ($ads_sql = mysqli_query($connection, "SELECT * FROM classifieds")) {
    printf("<h2>%d ads in All Classifieds</h2>", mysqli_num_rows($ads_sql));
}

$sql = "SELECT classifieds.id_classified as 'ID', category.id_category as 'CategoryID', category.name as 'Category', classifieds.name as 'Title', classifieds.description as 'Description', classifieds.image as 'Image', classifieds.price as 'Price' FROM classifieds
INNER JOIN category ON classifieds.id_category = category.id_category ORDER BY timestamp DESC";
$result = mysqli_query($connection,$sql) or die(mysqli_error($connection));
if(mysqli_num_rows($result)>0)
{
    echo "<table class=\"w3-table-all\">";
   
    while ($row=mysqli_fetch_array($result,MYSQLI_ASSOC))
    {
        echo "<tr>
                <td><a href=\"products.php?id_category=$row[CategoryID]\">$row[Category]</a></td>
                <td>".$row["Title"]."</td>
                <td>".$row["Description"]."</td>
                <td><img style=\"max-height: 150px\" src=\"".$row["Image"]."\"></td>
                <td>".$row["Price"]."</td>
              </tr>";

    }
    echo "</table><br>";

    mysqli_free_result($result);
}

mysqli_close($connection);
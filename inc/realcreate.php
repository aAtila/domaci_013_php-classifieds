<?php

  require("config.php");
  require("db_config.php");
  require("functions.php");

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $name = $_POST["name"];
  $category = $_POST["category"];
  $description = addslashes($_POST["description"]);
  $price = $_POST["price"];
}

if(isset($_FILES["file"]) AND is_uploaded_file($_FILES['file']['tmp_name'])) {

  $file_name = $_FILES['file']["name"];
  $file_temp = $_FILES["file"]["tmp_name"];
  $file_size = $_FILES["file"]["size"];
  $file_type = $_FILES["file"]["type"];
  $file_error = $_FILES['file']["error"];

  if ($file_error >0) {
    echo "Something went wrong during file upload!";
  }
  else {

    if (!exif_imagetype($file_temp)) {
      exit("File is not a picture!");
    }

    $ext_temp = explode(".", $file_name);
    $extension = end($ext_temp);

    $new_file_name = $ext_temp[0] . ".$extension";
    $directory = "../images";

    $upload = "$directory/$new_file_name";

    // upload fajla
    if (!is_dir($directory)) {
      mkdir($directory);
    }

    if (move_uploaded_file($file_temp, $upload)) {

    }
    else {
      echo "<p><b>Error!</b></p>";
    }

  }

}

$sql = "INSERT INTO classifieds (id_category, name, description, image, price)
        VALUES ('$category', '$name', '$description', 'images/$new_file_name', '$price')";

$result = mysqli_query($connection,$sql) or die(mysqli_error($connection));

header("Location: products.php?id_category=" . $category);


<?php
session_start();
require('config.php');
require('db_config.php');
require('functions.php');
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="PHP & MySQL domaci - Movies database">
        <meta name="keywords" content="php, functions, variables">
        <meta name="author" content="Atila Alacan">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Classifieds # PHP & MySQL domaci</title>
        <link href="https://fonts.googleapis.com/css?family=Fira+Mono|Fira+Sans" rel="stylesheet">
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/styles.css">
    </head>
    <body>
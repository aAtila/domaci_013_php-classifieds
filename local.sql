-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 14, 2018 at 09:37 AM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `local`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `id_category` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_category`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id_category`, `name`) VALUES
(1, 'pets'),
(2, 'home & garden'),
(3, 'sports'),
(4, 'tools & materials'),
(6, 'music'),
(7, 'appliances'),
(8, 'tech');

-- --------------------------------------------------------

--
-- Table structure for table `classifieds`
--

DROP TABLE IF EXISTS `classifieds`;
CREATE TABLE IF NOT EXISTS `classifieds` (
  `id_classified` int(11) NOT NULL AUTO_INCREMENT,
  `id_category` int(11) NOT NULL,
  `name` char(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `image` char(100) COLLATE utf8_unicode_ci NOT NULL,
  `price` float NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_classified`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `classifieds`
--

INSERT INTO `classifieds` (`id_classified`, `id_category`, `name`, `description`, `image`, `price`, `timestamp`) VALUES
(1, 1, 'Chinchilla', 'Beautiful female chinchilla looking for a new home where she will get plenty of love and time to play.', 'images/chinchilla.jpg', 60, '2018-04-13 22:54:13'),
(2, 2, 'Deliver Wooden Pallets Eur/Epal Home Beds Furnitur', 'CLEAN EURO PALLETS IN VERY GOOD CONDITION INDOoR OUTDOOR USE \r\n\r\nEur/Epal \r\nLength 1200mm (120cm) \r\nWidth 800mm (80cm) \r\nDepth 15cm \r\n\r\nStandard \r\nLength 1200mm (120cm) \r\nWidth 1000mm (100cm) \r\nDepth. 15cm \r\n\r\nDelivery £20-30 inside M25 \r\nCall for delivery outside of M25 \r\n\r\nImmediate delivery available - Call anytime \r\n\r\nSanding of Pallet and Cutting also \r\n\r\nContact: Email: Txt - Mark on 07547465236', 'images/wooden-pallets.jpg', 10, '2018-04-13 22:54:13'),
(3, 3, 'Vintage racer', 'Good condition. * * * * * * * * * * * * * *', 'images/vintage-racer.jpg', 45, '2018-04-13 22:54:13'),
(4, 4, 'Snap On 78 Tool Box - Limited Edition Extreme Green', 'Nearly new 78 stack Snap On tool box in extreme green. Purchased earlier this year but due to a recent promotion it will now be gathering dust.', 'images/snap-on-78-tool-box.jpg', 4200, '2018-04-13 22:54:13'),
(5, 7, 'LG Washing Machine (9kg) (6 Month Warranty)', 'Gorgeous LG Direct Drive washer. (8kg, 1400 spin, A+) \r\n\r\n- Factory Refurbished \r\n- Excellent Condition \r\n- 6 Month Warranty \r\n- Removal Of Old Appliance \r\n\r\nMany Makes and Models \r\n\r\n\r\nLiverpool Appliances \r\nUnit 3b, Dairy Business Park \r\nLong Lane \r\nL9 7BD \r\n\r\nMon-Fri: 9.30 - 5.30 \r\nSat: 10am - 4pm ', 'images/lg-washing-machine-9kg.jpg', 179, '2018-04-13 22:54:13'),
(6, 2, 'Glass and silver bowl', 'Glass bowl and silver base with handmade glass sweets very unusual came from italy.', 'images/glass-and-silver-bowl.jpg', 50, '2018-04-13 22:54:13'),
(7, 3, 'Used second hand and brand new Bike bicycle cycle bikes bicycles, new bike bicycle cycle for sale', 'Used Mountain Bikes from £35, City / Town Bikes from £49, clearance sale. \r\nWe also Fix Bikes in Competitive Price. \r\nLocation: \r\nStorage Number: X 22, At Ground Floor, \r\nBig Yellow Self Storage Ltd, Coral Park, Henley Road, Cambridge, CB1 3EA (Behind Homebase, End of Henley Road, after Royal Mail Parcel Force ) \r\nBefore buying Test ride welcome (Prefer after 11:30 am, or before 5: 45 pm) \r\nPhone: 07923512765 or 07856141045 \r\n\r\nLocal delivery Service including: CB1, CB2, CB3, CB4, CB5 only \r\nDelivery time: Within 2 working days, 5:45 - 7: 45 pm (Monday - Saturday) \r\nDelivery Fees: £3.50 (No refundable if Customer change their mind after placing a order. \r\nWe need your \r\n!) Phone No, 2) Full address, 3) Bike\' s No, 4) Pay £3.50 extra in advance by Paypal. Phone us to confirm this services.', 'images/used-bikes.jpg', 35, '2018-04-13 22:54:13'),
(8, 6, 'Song books', 'Show tunes and pop songs, Kate Bush, David Bowie,Natalie Merchant, Carol King...lots more! Condition used - some more than others - 16 books for £30', 'images/song-book.jpg', 30, '2018-04-13 22:54:13'),
(10, 8, 'Sony a6500 with 18 105f4', 'The body bought in Curry\'s 6months ago, the lens bought in Harrison Sheffield, with receipt. And original case and all accessories. No scratch like a new.', 'images/sony-a6500.jpg', 1150, '2018-04-14 00:11:23');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id_role` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  PRIMARY KEY (`id_role`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id_role`, `name`) VALUES
(1, 'admin'),
(2, 'user');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `lastname` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `id_role` int(11) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `username`, `password`, `email`, `firstname`, `lastname`, `id_role`) VALUES
(1, 'admin', '6bde3839b510084c1f8b3bf6a46c04a7', 'john@doe.com', 'John', 'Doe', 1),
(2, 'user', '6bde3839b510084c1f8b3bf6a46c04a7', 'jane@doe.com', 'Jane', 'Doe', 2);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
